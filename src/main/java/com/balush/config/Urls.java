package com.balush.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@AllArgsConstructor
public class Urls {

    private final DataProperties dataProperties;

    private final static String API = "/api";
    private final static String HTTP = "http://";
    private final static String DATA_GATEWAY = "/gateway";
    private final static String LOCALHOST = "localhost:";
    public static String DATA_GATEWAY_SERVICE_URL = "";

    @PostConstruct
    private void initRoutes() {
        DATA_GATEWAY_SERVICE_URL = HTTP + LOCALHOST + dataProperties.getGateway() + API + DATA_GATEWAY;
    }

}
