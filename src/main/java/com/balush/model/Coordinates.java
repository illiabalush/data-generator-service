package com.balush.model;

import lombok.Builder;
import lombok.ToString;

import java.io.Serializable;

@Builder
@ToString
@lombok.Data
public class Coordinates implements Serializable {

    private float latitude;

    private float longitude;

}
