package com.balush.model;

import lombok.*;

@Builder
@lombok.Data
@ToString
public class Data {

    private String type;

    private Float value;

    private Coordinates coordinates;

    private long timestamp;

}
