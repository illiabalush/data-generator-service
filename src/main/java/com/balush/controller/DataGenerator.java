package com.balush.controller;

import com.balush.model.Data;

public interface DataGenerator {

    Data generateData();

}
