package com.balush.controller;

import com.balush.config.Urls;
import com.balush.model.Data;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
@Component
public class DataScheduler {

    private final RestTemplate restTemplate;
    private final DataGenerator dataGenerator;

    @Async
    @Scheduled(fixedRate = 1000)
    public void sendIncomingData() {
        Data data = dataGenerator.generateData();
        System.out.println("Generate data: " + data);
        restTemplate.postForEntity(Urls.DATA_GATEWAY_SERVICE_URL, data, Data.class);
    }

}
