package com.balush.controller;

import com.balush.config.DataType;
import com.balush.model.Coordinates;
import com.balush.model.Data;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class DataGeneratorImpl implements DataGenerator {

    private static Random random = new Random();

    @Override
    public Data generateData() {
        DataType dataType = getRandomType();
        return Data
                .builder()
                .type(dataType.getDataName())
                .coordinates(getRandomCoordinates())
                .value(getRandomValue(dataType).floatValue())
                .timestamp(System.currentTimeMillis())
                .build();
    }

    private DataType getRandomType() {
        int typeNumber = getRandomNumberInts(0, DataType.values().length - 1);
        return DataType.values()[typeNumber];
    }

    private Coordinates getRandomCoordinates() {
        return Coordinates
                .builder()
                .latitude(getRandomNumberInts(-90, 90))
                .longitude(getRandomNumberInts(-180, 180))
                .build();
    }

    private Double getRandomValue(DataType dataType) {
        if (dataType == DataType.HUMIDITY) {
            return getRandomNumberDouble(0.0, 1.0);
        } else if (dataType == DataType.TEMPERATURE) {
            return getRandomNumberDouble(-70.0, 70.0);
        } else {
            return getRandomNumberDouble(0.0, 50.0);
        }
    }

    public static int getRandomNumberInts(int min, int max) {
        return random.ints(min, (max + 1)).findFirst().getAsInt();
    }

    public static double getRandomNumberDouble(double min, double max) {
        return random.doubles(min, max).findFirst().getAsDouble();
    }
}
